def answer_content_serializer(answer) -> dict:
    return {
        'answer_content': str(answer['answer_content'])
    }


def answers_content_serializer(answers) -> list:
    return [answer_content_serializer(answer) for answer in answers]
