def question_serializer(question) -> dict:
    return {
        "question_content": question['question_content'],
    }


def questions_serializer(questions) -> list:
    return [question_serializer(question) for question in questions]