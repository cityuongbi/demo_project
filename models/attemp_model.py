from typing import Optional
from pydantic import BaseModel


class Attemp(BaseModel):
    test_id: str
    user_id: str
    question_ids: list
    answers: list
    score: int
    start_time: str
    time_remaining: Optional[int] = 0