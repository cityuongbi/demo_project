from datetime import datetime, date
from typing import List
from bson import ObjectId
from fastapi import APIRouter, Depends
from models.test_detail_model import TestDetail
from models.test_model import Test, AddTest
from schemas.test_schema import tests_serializer, test_serializer, test_detail_serializer
from config.database import test_collection, topic_detail_collection, test_detail_collection, question_collection, \
    answer_collection, attemp_collection
from routers.authentication_router import get_current_user
from schemas.answer_schema import answer_content_serializer
from models.attemp_model import Attemp
from schemas.attemp_schema import attemp_serializer

router = APIRouter(
    prefix='/test',
    tags=['Test']
)


@router.get('/{topic_id}')
def get_all_test(topic_id: str):
    topic_details = topic_detail_collection.find({"topic_id": topic_id})
    test_ids = [ObjectId(topic_detail["test_id"]) for topic_detail in topic_details]
    tests = test_collection.find({"_id": {"$in": test_ids}})

    if not tests:
        return {
            'status': 404,
            'data': 'Test not found!'
        }

    return {
        "status": 200,
        "data": tests_serializer(tests)
    }


@router.get('/detail/{test_id}')
def get_test_by_id(test_id: str, current_user=Depends(get_current_user)):
    test = test_collection.find_one({'_id': ObjectId(test_id)})

    if not test:
        return {
            'status': 404,
            'data': 'Test not found!'
        }

    start_date = datetime.strptime(test['start_date'] + ' - ' + test['start_time'], '%d/%m/%Y - %H:%M').date()
    end_date = datetime.strptime(test['end_date'] + ' - ' + test['end_time'], '%d/%m/%Y - %H:%M').date()
    current_datetime = datetime.now()
    current_date = current_datetime.date()

    if current_user.role == 'Admin' or (start_date <= current_date <= end_date):

        test_details = test_detail_collection.find({'test_id': test_id})

        questions = []

        for test_detail in test_details:
            question = {
                'question_id': test_detail['question_id'],
                'question_content': question_collection.find_one({'_id': ObjectId(test_detail['question_id'])})[
                    'question_content'],
                'answers': []
            }

            answers = answer_collection.find({'question_id': test_detail['question_id']})

            for answer in answers:
                question['answers'].append(answer_content_serializer(answer))

            questions.append(question)

        test['questions'] = questions

        return {
            'status': 200,
            'data': test_detail_serializer(test)
        }

    return {
        'status': 403,
        'data': 'Ngoài thời gian làm bài!'
    }


@router.post('/detail/get_attemp')
def add_attemp(attemp: Attemp, current_user=Depends(get_current_user)):
    if current_user.role == 'Admin':
        return {
            'status': 200
        }

    attemp_exits = attemp_collection.find_one({'user_id': current_user.user_id, 'test_id': attemp.test_id})

    if not attemp_exits:
        attemp_collection.insert_one(attemp.dict(by_alias=True))
        start_time = datetime.strptime(attemp.start_time, '%d/%m/%Y - %H:%M:%S')
    else:
        start_time = datetime.strptime(attemp_exits['start_time'], '%d/%m/%Y - %H:%M:%S')

    test = test_collection.find_one({'_id': ObjectId(attemp.test_id)})

    time_remaining = start_time - datetime.now()

    if time_remaining.total_seconds() + test['duration'] * 60 <= 0:
        return {
            'status': 403,
            'data': 'Ngoài thời gian làm bài!'
        }

    attemp.time_remaining = time_remaining.total_seconds() + test['duration'] * 60

    return {
        'status': 200,
        'data': attemp
    }


@router.post('/add_test')
def add_test(test: AddTest, current_user=Depends(get_current_user)):
    if current_user.role != 'Admin':
        return {
            'status': 401,
            'data': 'You are not authorized to access this page.'
        }

    _id = test_collection.insert_one(test.dict(by_alias=True))

    topic_detail = {
        'topic_id': str(test.topic_id),
        'test_id': str(_id.inserted_id)

    }

    topic_detail_collection.insert_one(topic_detail)

    return {
        'status': 200,
        'data': 'Test added successfully!'
    }


@router.post('/add_question')
def add_question(request: TestDetail, current_user=Depends(get_current_user)):
    if current_user.role != 'Admin':
        return {
            'status': 401,
            'data': 'You are not authorized to access this page.'
        }

    test_details = [{'test_id': request.test_id, 'question_id': item} for item in request.question_ids]

    test_detail_collection.insert_many(test_details)  # bulk_write

    return {
        'status': 200,
        'data': 'Add question successfully!'
    }
