from fastapi import APIRouter, Depends
from models.question_model import Question, QuestionDetail
from routers.authentication_router import get_current_user
from config.database import question_collection, answer_collection

router = APIRouter(
    prefix='/question',
    tags=['Question']
)


@router.post('/add_question')
def add_question(request: QuestionDetail, current_user=Depends(get_current_user)):
    if current_user.role != 'Admin':
        return {
            'status': 401,
            'data': 'You are not authorized to access this page.'
        }

    question = {
        'question_content': request.question_content
    }

    _id = question_collection.insert_one(question)

    for item in request.answers:
        answer = {
            'question_id': str(_id.inserted_id),
            'answer_content': item.answer_content,
            'is_correct': item.is_correct
        }

        answer_collection.insert_one(answer)

    return {
        'status': 200,
        'data': 'Add question successfully!'
    }
