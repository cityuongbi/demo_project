import json
from config.redis_config import rd
from fastapi import APIRouter, Depends
from models.topic_model import Topic
from schemas.topic_schema import topics_serializer, topic_serializer
from config.database import topic_collection, test_detail_collection, topic_detail_collection
from routers.authentication_router import get_current_user

router = APIRouter(
    prefix='/topic',
    tags=['Topic']
)


@router.get('')
def get_all_topic():
    topics = rd.get('topics')

    if not topics:
        topics = topics_serializer(topic_collection.find())

        for topic in topics:
            topic['num_of_test'] = topic_detail_collection.count_documents({'topic_id': topic['topic_id']})

        rd.set('topics', json.dumps(topics))
        rd.expire('topics', 30)
    else:
        topics = json.loads(topics)

    return {
        'status': 200,
        'data': topics
    }


@router.get('/{topic_id}')
def get_topic_by_id(topic_id: str):
    topic = topic_collection.find_one({'_id': topic_id})
    if topic:
        return {
            'status': 200,
            'data': topic_serializer(topic)
        }
    return {
        'status': 404,
        'data': 'Topic not found!'
    }


@router.post('/add_topic')
def add_topic(topic: Topic, current_user=Depends(get_current_user)):
    if current_user.role != 'Admin':
        return {
            'status': 401,
            'data': 'You are not authorized to access this page.'
        }

    topic_collection.insert_one(topic.dict(by_alias=True))

    return {
        'status': 200,
        'data': 'Topic added successfully!'
    }
